let express = require('express');
let https = require('https');
let fs = require('fs');

let middleware = require('./middleware');
let routes = require('./routes');

let app = express();

middleware(app);
routes(app);

let key = fs.readFileSync('./server.key');
let cert = fs.readFileSync('./server.crt');

let options = {
    key: key,
    cert: cert,
};

const PORT = 8080;
const HOST = 'localhost';

https.createServer(options, app).listen(PORT, HOST, (err) => {
    console.log('Server started at %s:%s', HOST, PORT); // eslint-disable-line
});
