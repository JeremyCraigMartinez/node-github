let request = require('request');

const GITHUB_URL = 'https://api.github.com';

/* Cannot retrieve project info from id because projects is an authentication based call
 *
 * Example:
 * curl -H "Accept: application/vnd.github.inertia-preview+json" -u user:token https://api.github.com/repos/:user/:project/projects
 *
 * Projects API is under development, temporary Accept header is required
 * Read more: https://developer.github.com/v3/projects/
 *
 * When endpoint is tried without authentication, this response is given:
 * {
 *   "message": "Requires authentication",
 *   "documentation_url": "https://developer.github.com/v3"
 * }
 */

function sendRequest(path) {
    return new Promise((resolve, reject) => {
        let options = {
            url: `${GITHUB_URL}${path}`,
            headers: {
                'User-Agent': 'request',
                Accept: 'application/json',
            },
        };
        request(options, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                resolve(body);
            }
        });
    });
}

module.exports = (app) => {
    app.get('/github/:user', (req, res, next) => {
        let user = sendRequest(`/users/${req.params.user}`);
        let repos = sendRequest(`/users/${req.params.user}/repos`);

        Promise.all([user, repos])
            .then((responseData) => {
                res.status(200).send(responseData);
            })
            .catch((error) => {
                console.log(error); // eslint-disable-line
                res.status(400).send('something went wrong');
            });
    });
};
