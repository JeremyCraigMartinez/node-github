##Installation

You will need [node](https://nodejs.org) v6 for this

```
git clone https://bitbucket.org/JeremyCraigMartinez/node-github
npm install
# generate a self signed cert. Simple instructions found below
# https://devcenter.heroku.com/articles/ssl-certificate-self
```

##Usage

`node index.js`

Then pull up https://localhost:8080/github/:user in your browser

##Linting

`npm run lint`

##Test

`npm test`